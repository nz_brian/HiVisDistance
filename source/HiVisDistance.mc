using Toybox.Application as App;
using Toybox.WatchUi as Ui;
using Toybox.Graphics as Graphics;
using Toybox.System as System;

//! @author Brian Bannister
class HiVisDistance extends App.AppBase {
	hidden var view;
	
	function initialize() {
        AppBase.initialize();
    }

    function getInitialView() {
        view = new HiVisDistanceView();
        onSettingsChanged();
        return [ view ];
    }
    
    
    function onSettingsChanged() { 
    	System.println("onSettingsChanged()");
    }
}


//! @author Brian Bannister
class HiVisDistanceView extends Ui.DataField {

    hidden const RIGHT = Graphics.TEXT_JUSTIFY_RIGHT | Graphics.TEXT_JUSTIFY_VCENTER;
    hidden const LEFT = Graphics.TEXT_JUSTIFY_LEFT | Graphics.TEXT_JUSTIFY_VCENTER;
    hidden const CENTER = Graphics.TEXT_JUSTIFY_CENTER | Graphics.TEXT_JUSTIFY_VCENTER;
    
    hidden const ZERO_DISTANCE = "0.00";
    
    hidden const RED = Graphics.COLOR_RED;
    hidden const YELLOW = Graphics.COLOR_YELLOW;
    hidden const GREEN = Graphics.COLOR_GREEN;
    hidden const DK_GREEN = Graphics.COLOR_DK_GREEN;
    hidden const BLUE = Graphics.COLOR_BLUE;
    hidden const WHITE = Graphics.COLOR_WHITE;
    hidden const BLACK = Graphics.COLOR_BLACK;
    hidden const GRAY = Graphics.COLOR_LT_GRAY;
    hidden const DK_GRAY = Graphics.COLOR_DK_GRAY;
    hidden const TRANSPARENT = Graphics.COLOR_TRANSPARENT;
    
    hidden var kmOrMileInMeters = 1000;
    hidden var distanceUnits = System.UNIT_METRIC;
    hidden var textColor = BLACK;
    hidden var inverseTextColor = WHITE;
    hidden var backgroundColor = WHITE;
    hidden var inverseBackgroundColor = BLACK;
    hidden var headerColor = DK_GRAY;
    
    hidden var distance = 0;
    
    function initialize() {
        DataField.initialize();
    }

    function compute(info) {      
        distance = info.elapsedDistance != null ? info.elapsedDistance : 0;
    }
    
    function onLayout(dc) {
        setDeviceSettingsDependentVariables();
        onUpdate(dc);
    }
    
    function onUpdate(dc) {
        setColors();
        // reset background
        dc.setColor(backgroundColor, backgroundColor);
        dc.fillRectangle(0, 0, 218, 218);
        
        drawValues(dc);
    }
     
    function drawValues(dc) {
        
        // DISTANCE
        var distStr = getDistance();

		// LAYOUT
        dc.setColor(textColor, TRANSPARENT);
	    dc.drawText(200, 38, Graphics.FONT_NUMBER_THAI_HOT, distStr, RIGHT);

        dc.setColor(headerColor, TRANSPARENT);
        if (distStr.length() < 5) {	        
	        dc.drawText(4, 32, Graphics.FONT_LARGE, "Distance", LEFT);          
        }
        if (distStr.length() < 6) {	        
	        dc.drawText(4, 32, Graphics.FONT_LARGE, "Dist", LEFT);          
        }
        else {
	        dc.drawText(4, 32, Graphics.FONT_MEDIUM, "Dist", LEFT);   
        }
    }
    
    function getDistance() {
    	var distStr = ZERO_DISTANCE;
        if (distance > 0) {
            var distanceKmOrMiles = (distance / kmOrMileInMeters);
            
            //distanceKmOrMiles += 995;
            
            distStr = distanceKmOrMiles.format("%.2f");
        }
        return distStr;
    }
    
    
    function setColors() {
    	var colourSetting = getBackgroundColor();
        if (null != colourSetting && colourSetting != backgroundColor) {
            backgroundColor = colourSetting;
            textColor = (backgroundColor == BLACK) ? WHITE : BLACK;
            inverseTextColor = (backgroundColor == BLACK) ? WHITE : WHITE;
            inverseBackgroundColor = (backgroundColor == BLACK) ? DK_GRAY: BLACK;
            headerColor = (backgroundColor == BLACK) ? GRAY: DK_GRAY;
        }
    }
    

    function setDeviceSettingsDependentVariables() {
        var deviceSettings = System.getDeviceSettings();
        
        distanceUnits = deviceSettings.distanceUnits;
        if (distanceUnits == System.UNIT_METRIC) {
            kmOrMileInMeters = 1000;
        } else {
            kmOrMileInMeters = 1610;
        }
        
    }
}
